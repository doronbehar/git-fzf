#!/bin/sh

_usage() {
	cat << EOF
usage: git fzf <subcommand> <arguments>

  log     Like git log, but with a graph and a fzf's preview window showing the
          changes of the highlighted commit.
          Keybindings:
             - <V> : View the whole diff of the highlighted commit with \$PAGER.
             - <Y> : Copy to clipboard the ref of the current.
             - <enter>  : Print the selected commits' hashes to stdout.
          All arguments to this command pass on to git log.
  stash   Like git stash list, but with fzf's preview window showing the changes of
          the highlighted stash.
          Keybindings:
             - <V> : View the whole diff of the highlighted commit with \$PAGER.
             - <Y> : Copy to clipboard the ref of the current.
             - <D> : Delete (drop) the highlighted stash (it'll ask for confirmation).
             - <A> : Apply the highlighted stash.
             - <P> : Apply the highlighted stash and remove it from the stash list (pop).
             - <enter>  : Print the selected stashes' hashes to stdout.
          All arguments to this command pass on to git stash list.
EOF
}

# `_command_exists`: Show if a command exists
# Taken from http://stackoverflow.com/a/592649/4935114
_command_exists () {
	(type "$1" 2>&1) > /dev/null ;
}

if [ -z "$1" ]; then
	echo git-fzf: no subcommand supplied\! 2>&1
	_usage
	exit 2
fi

case "$(uname)" in
	Linux)
		# Why different xargs for every OSTYPE?
		# See this answer: https://stackoverflow.com/a/19038748/4935114
		xargs="xargs -r"
		if [ -z "$GIT_FZF_CLIPBOARD_COMMAND" ]; then
			if _command_exists wl-copy; then
				clip="wl-copy"
			else
				for exe in xsel xclip; do
					if _command_exists $exe; then
						clip="${GIT_FZF_CLIPBOARD_COMMAND:-$exe -i}"
						break
					fi
				done
			fi
			if [ -z "$clip" ]; then
				echo "git-fzf: Warning: Could not found either of the executables:" >&2
				echo "         - xsel" >&2
				echo "         - xclip" >&2
				echo "         - wl-copy" >&2
				echo "         Copying to clipboard is not supported" >&2
				clip=true
			fi
		else
			clip=$GIT_FZF_CLIPBOARD_COMMAND
		fi
		;;
	Darwin)
		xargs="xargs -n1"
		if [ -z "$GIT_FZF_CLIPBOARD_COMMAND" ]; then
			if _command_exists pbcopy; then
				clip=pbcopy
			else
				echo "git-fzf: Warning: Could not found pbcopy executable, copying" >&2
				echo "         to clipboard is not supported" >&2
				clip=true
			fi
		else
			clip=$GIT_FZF_CLIPBOARD_COMMAND
		fi
		;;
esac

subcommand="$1"
shift
PAGER="$GIT_PAGER"
if [ -z "$PAGER" ]; then
	PAGER=less
fi
export PAGER

case "$subcommand" in
	"log")
		# `--graph` and `--no-walk` collide
		if echo "$*" | grep -q -- --no-walk || echo "$*" | grep -q -- --reverse; then
			sed_refs_filter='s/\([a-f0-9]\+\).*/\1/p'
		else
			graph='--graph'
			sed_refs_filter='s/[*\| ]\+ \([a-f0-9]\+\).*/\1/p'
		fi
		# I don't think it is possible to quote this and avoid this warning:
		# shellcheck disable=SC2016
		get_ref='printf "%s" $(echo {} | sed -n "'"${sed_refs_filter}"'" )'
		preview_command="${get_ref} | $xargs git show --color=always"
		git log "$graph" --color=always --format='%C(auto)%h%d %s %C(black)%C(bold)%cr' "$@" | \
			fzf -m +s --ansi \
			--bind="V:execute(LESS=-R $PAGER -f <(echo {} | ${preview_command}) < /dev/tty > /dev/tty 2>&1)" \
			--bind="Y:execute-silent(${get_ref} | $clip)" \
			--preview="$preview_command" \
			| sed -n "${sed_refs_filter}"
		;;
	"stash")
		sed_refs_filter='s/stash@{\([0-9]\+\)}: .*/\1/p'
		# we split the words ${sed} intentionally, hence:
		# shellcheck disable=SC2016
		get_ref='printf "%s" $(echo {} | sed -n "'"${sed_refs_filter}"'" )'
		preview_command="${get_ref} | $xargs git stash show --color=always"
		# Quoting is super delicate here, hence:
		# shellcheck disable=SC2068,SC2027,SC2145
		git stash list "$@" | \
			fzf -m +s --ansi \
			--bind="V:execute(LESS=-R $PAGER -f <(echo {} | ${preview_command}) < /dev/tty > /dev/tty 2>&1)" \
			--bind="Y:execute-silent(${get_ref} | $clip)" \
			--bind="D:execute(git fzf _stash-op drop \$(${get_ref}) < /dev/tty > /dev/tty 2>&1)+reload(git stash list "$@")" \
			--bind="A:execute(git fzf _stash-op apply \$(${get_ref}) < /dev/tty > /dev/tty 2>&1)+reload(git stash list "$@")" \
			--bind="P:execute(git fzf _stash-op pop \$(${get_ref}) < /dev/tty > /dev/tty 2>&1)+reload(git stash list "$@")" \
			--preview="$preview_command" \
			| sed -n "${sed_refs_filter}"
		;;
	# used internally by git stash
	"_stash-op")
		op="$1"
		ref="$2"
		git -c core.pager=cat stash show --patch-with-stat "$2"
		printf '%s\n%s' "==================================" "git-fzf: Are you sure you wish to $op this stash (stash@{$ref})? [y/n]: "
		read -r ans
		case $ans in
			[yY]*)
				# echo git stash $op "$ref"
				if ! git stash "$op" "$ref"; then
					printf '%s\n%s' \
						"==================================" \
						"git-fzf: failed to $op this stash (stash@{$ref}), press any key to continue"
					# shellcheck disable=SC2034
					read -r n
				fi
				;;
			*)
				echo git-fzf: aborting
				;;
		esac
		;;
	*)
		echo git-fzf: unsupported sub command: "$subcommand" 2>&1
		_usage
		exit 2
		;;
esac

# vim:ft=sh:tabstop=4:shiftwidth=4
